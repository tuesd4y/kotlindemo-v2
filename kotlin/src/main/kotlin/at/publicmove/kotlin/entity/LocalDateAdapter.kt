package at.publicmove.kotlin.entity

import java.time.LocalDate
import javax.xml.bind.annotation.adapters.XmlAdapter


/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 25/02/2017
 */

class LocalDateTimeXmlAdapter : XmlAdapter<String, LocalDate>() {
    override fun marshal(entityField: LocalDate): String = entityField.toString()

    override fun unmarshal(xmlField: String): LocalDate = LocalDate.parse(xmlField)
}