@XmlJavaTypeAdapters(@XmlJavaTypeAdapter(type = LocalDate.class, value = LocalDateTimeXmlAdapter.class))
package at.publicmove.kotlin.entity;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import java.time.LocalDate;