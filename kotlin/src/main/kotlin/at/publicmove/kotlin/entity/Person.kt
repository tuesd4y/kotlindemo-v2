
package at.publicmove.kotlin.entity

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.xml.bind.annotation.XmlTransient

//region Person
/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 25/02/2017
 * updated on 26/02/2017 :: added gender property
 */

data class Person(
        val id: Int = 0,
        val firstName: String = "",
        val lastName: String = "",
        val birthDate: LocalDate = LocalDate.MIN,
        val male: Boolean = false
) {
    companion object {
        val dtf: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yy")
    }
    constructor(id: String, firstName: String, lastName: String, birthDate: String, gender: String)
            : this(id.toInt(), firstName, lastName, LocalDate.parse(birthDate, dtf).minusYears(100), gender == "m")

    @get:XmlTransient
    val isFemale: Boolean
        get() = !male
}

//endregion

