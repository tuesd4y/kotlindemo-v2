package at.publicmove.kotlin.data

//import at.publicmove.kotlin.entity.Person
import at.publicmove.kotlin.entity.Person
import java.nio.charset.Charset
import java.util.stream.Collectors
import javax.ws.rs.core.UriInfo
import kotlin.reflect.full.memberProperties

// region PersonManager
///**
// * @author Christopher Stelzmüller
// * @version 1.0
// * created on 25/02/2017
// * updated on 26/02/2017 :: added option to select only male/female persons
// *      by passing the respective gender-property as a queryParam
// * updated on 26/02/2017 :: added minus operator function
// */

typealias PropertySelector<T> = (T) -> Comparable<Any>?
typealias Predicate<T> = (T) -> Boolean

object PersonManager {
    private val persons = loadPersons()

    operator fun plus(p: Person) {
        if(persons.none { it.id == p.id }) {
            persons.add(p)
        } else {
            throw Exception("There is already a person with the id ${p.id} in the list.")
        }
    }

    operator fun minus(id: Int) = persons.removeAll { it.id == id }

    operator fun get(id: Int): Person = persons.first { it.id == id }

    fun get(uriInfo: UriInfo): List<Person> {
        val directionAscending = uriInfo.queryParameters.getFirst("dir") != "desc"
        val order = uriInfo.queryParameters.getFirst("order") ?: "id"
        val gender = uriInfo.queryParameters.getFirst("gender")

        val sel: PropertySelector<Person> = selector(order)

        val filterPredicate: Predicate<Person> = when(gender) {
            "male" -> Person::male
            "female" -> Person::isFemale
            else -> { _ -> true }
        }

        return persons.filter(filterPredicate).let {
            if(directionAscending)
                it.sortedBy(sel)
            else
                it.sortedByDescending(sel)
        }

    }

    @Suppress("UNCHECKED_CAST")
    private fun selector(propName: String): PropertySelector<Person> {
        return {  person ->
            (person::class.memberProperties.first { prop ->
                prop.name.equals(propName, ignoreCase = true)
            }(person)?: person.id) as Comparable<Any>
        }
    }

    private fun loadPersons() = this::class
            .java
            .getResourceAsStream("/persons.csv")
            .bufferedReader()
            .lines()
            .skip(1)
            .map { it.split(";") }
            .map { Person(it[1], it[3], it[2], it[4], it[5]) }
            .collect(Collectors.toList())
}
// endregion