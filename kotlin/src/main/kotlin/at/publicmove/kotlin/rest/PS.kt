package at.publicmove.kotlin.rest

import at.publicmove.kotlin.data.PersonManager
import at.publicmove.kotlin.entity.Person
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.UriInfo


//region PS
/**
 * @author Christopher Stelzmüller
 * @version 1.0
 * created on 25/02/2017
 * updated on 26/02/2017 :: added the delete-REST-method
 */

//@Path("persons")
open class PS {

    val personManager = PersonManager

    @Context
    lateinit private var uriInfo: UriInfo


    @DELETE
    @Path("{id}")
    fun delete(@PathParam("id") id: Int) = personManager - id

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun get() = personManager.get(uriInfo)

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    fun get(@PathParam("id") id: Int) = personManager[id]

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    fun post(newPerson: Person): Person {
        personManager + newPerson
        return newPerson
    }
}
//endregion