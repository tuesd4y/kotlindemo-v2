package at.publicmove.kotlin.config

import javax.ws.rs.ApplicationPath
import javax.ws.rs.core.Application

/**
* @author Christopher Stelzmüller
* @version 1.0
* created on 25/02/2017
*/

@ApplicationPath("rs")
open class RestConfig : Application()