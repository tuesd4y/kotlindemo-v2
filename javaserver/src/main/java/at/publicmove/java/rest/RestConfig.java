package at.publicmove.java.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by Me on 14/12/2016.
 */
@ApplicationPath("rs")
public class RestConfig extends Application {
}