package at.publicmove.java.rest;

import at.publicmove.java.facade.PersonFacade;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.Objects;

/**
 * Created by Me on 26/02/2017.
 */
@Stateless
@Path("persons")
public class PersonResource {

    @Inject
    PersonFacade personFacade;

    @GET
    @Produces({"application/json"})
    public Response getAll(){

        return Response.status(200).entity(personFacade.loadData()).build();
    }

    @GET
    @Produces({"application/json"})
    @Path("gender/{gender}")
    public Response getAll(@PathParam("gender") String gender){

        return Response.status(200).entity(personFacade.getByGender(Objects.equals(gender, "m"))).build();
    }
}
