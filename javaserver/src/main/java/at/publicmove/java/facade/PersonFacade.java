package at.publicmove.java.facade;

import at.publicmove.java.entity.Person;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Me on 26/02/2017.
 */
@Stateless
public class PersonFacade{

    public static final String FILE_NAME = "/persons.csv";
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yy");

    private List<Person> persons = loadData();

    public List<Person> loadData(){

        List<Person> persons = new LinkedList<>();

        new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(FILE_NAME), Charset.defaultCharset()))
                .lines()
                .skip(1)
                .map(s -> s.split(";"))
                .map(a -> new Person(Long.parseLong(a[1]), a[3], a[2], LocalDate.parse(a[4], DATE_FORMATTER).minusYears(100), Objects.equals(a[5], "m")))
                .forEach(persons::add);

        return persons;
    }

    public List<Person> getAll(){
        return persons;
    }

    public List<Person> getByGender(boolean isMale){

        List<Person> listOfPersons = new LinkedList<>();

        persons.forEach(person -> {
            if(person.getMale() == isMale){
                listOfPersons.add(person);
            }
        });

        return listOfPersons;

    }


}
