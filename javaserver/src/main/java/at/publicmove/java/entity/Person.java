package at.publicmove.java.entity;

import at.publicmove.java.entity.converter.LocalDateDeserializer;
import at.publicmove.java.entity.converter.LocalDateSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.Entity;
import java.time.LocalDate;

/**
 * Created by Me on 26/02/2017.
 */
@Entity
public class Person {

    private Long id;

    private String firstName;

    private String lastName;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(pattern = "dd.MM.yy")
    private LocalDate birthdate;

    private Boolean isMale;

    public Person() {
    }

    public Person(Long id, String firstName, String lastName, LocalDate birthdate, Boolean isMale) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthdate = birthdate;
        this.isMale = isMale;
    }

    public Long getId() {
        return id;
    }

    public Person setId(Long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Person setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Person setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public Person setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
        return this;
    }

    public Boolean getMale() {
        return isMale;
    }

    public Person setMale(Boolean male) {
        isMale = male;
        return this;
    }
}
