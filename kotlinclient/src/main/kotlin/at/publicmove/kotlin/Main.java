package at.publicmove.kotlin;

import at.publicmove.kotlin.entity.Person;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import sun.rmi.runtime.Log;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import static com.fasterxml.jackson.module.kotlin.ExtensionsKt.jacksonObjectMapper;

/**
 * Created by Me on 27/02/2017.
 */
public class Main {

    public static String getJSONString(String urlString){
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String result = "";
        try {
            URL url = new URL(urlString);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                return null;
            }
            result = buffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally{
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;

    }

    public static List<Person> convertToPersons(String jsonString){
        ObjectMapper mapper = jacksonObjectMapper();
        List<Person> personList = null;

        mapper.findAndRegisterModules();
        try {
            personList = mapper.readValue(jsonString, new TypeReference<List<Person>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return personList;
    }

    public static void main(String[] args) {

        String url = "";
        Scanner in = new Scanner(System.in);


        System.out.print("Wollen Sie alle Frauen oder Männer anzeigen? [M,F]: ");
        String gender = Objects.equals(in.next(), "M") ? "male" : "female";
        System.out.print("Soll die Liste aufsteigend oder absteigend sotiert werden? [asc, desc]: ");
        String dir = in.next();
        System.out.print("Nach welchem Attribut soll die Liste sotiert werden? [attribute, none]: ");
        String sorting = in.next();

        if(sorting.equals("none")){
            url = "http://localhost:8080/ps/rs/persons?" + "&dir=" + dir + "&gender=" + gender;
        }else {
            url = "http://localhost:8080/ps/rs/persons?" + "order=" + sorting + "&dir=" + dir + "&gender=" + gender;
        }

        String jsonString = getJSONString(url);
        List<Person> personList = convertToPersons(jsonString);

        for (Person person : personList) {
            System.out.println(person);
        }
    }

}
