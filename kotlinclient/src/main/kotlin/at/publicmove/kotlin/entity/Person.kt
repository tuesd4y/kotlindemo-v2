
package at.publicmove.kotlin.entity

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Created by Me on 26/02/2017.
 */

data class Person(
        val id: Int = 0,
        val firstName: String = "",
        val lastName: String = "",
        @JsonSerialize(using = ToStringSerializer::class)
        @JsonDeserialize(using = ParseDeserializer::class)
        val birthDate: LocalDate = LocalDate.MIN,
        val male: Boolean = true
) {
    companion object {
        val dtf: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yy")
    }
    constructor(id: String, firstName: String, lastName: String, birthDate: String, male: Boolean)
            : this(id.toInt(), firstName, lastName, LocalDate.parse(birthDate, dtf).minusYears(100), male)
}

internal class ParseDeserializer : StdDeserializer<LocalDate>(LocalDate::class.java) {

@Throws(IOException::class, JsonProcessingException::class)
override fun deserialize(p: JsonParser, ctx: DeserializationContext): LocalDate {
    return LocalDate.parse(p.valueAsString) // or overloaded with an appropriate format
}
}