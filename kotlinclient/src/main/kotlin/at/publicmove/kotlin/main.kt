package at.publicmove.kotlin

import at.publicmove.kotlin.entity.Person
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.net.URL
import java.time.LocalDateTime
import com.fasterxml.jackson.core.JsonProcessingException
import java.io.IOException
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import java.util.*

fun String.getJSON(): String {
    val result = URL(this).readText()
    return result
}

fun String.convertToPersons(): List<Person> {

    val mapper = jacksonObjectMapper()
    mapper.findAndRegisterModules()
    val persons: List<Person> = mapper.readValue(this)

    return persons
}

private fun <E> List<E>.writeLine(range: Int) {

    /* not fancy

    for (i in 1..range) {
        if(this.size > i)
        println(this[i-1]);
    }

     */

    take(range).forEach(::println)
}

fun main(args: Array<String>) {

    //http://localhost:8080/ps/rs/persons?order=birthdate&dir=asc

    print("Wollen Sie alle Frauen oder Männer anzeigen? [male,female]: ")
    val input = Scanner(System.`in`)
    val gender = input.next()
    val genderParam = if(gender == "male" || gender == "female") "&gender=$gender" else ""
    print("Soll die Liste aufsteigend oder absteigend sotiert werden? [asc, desc]: ")
    val dir = input.next();
    print("Nach welchem Attribut soll die Liste sotiert werden? [<attribute>, <empty>]: ")
    val sorting = input.next()
    val sortingParam = if(sorting != "none") "order=$sorting" else ""
    print("Wie viele Personen sollen maximal angezeigt werden? : ")
    val range = input.nextInt();

    "http://localhost:8080/ps/rs/persons?$sortingParam&dir=$dir$genderParam".getJSON().convertToPersons().writeLine(range)
}